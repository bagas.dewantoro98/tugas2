import React from 'react';
import type {Node} from 'react';
import {
  Image,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Register from './pages/Register';
import Login from './pages/Login';
import Forgot from './pages/Forgot';
import Home from './pages/Home';
import BotTabs from './pages/BotTabs';

const StackNav = createNativeStackNavigator();

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <StackNav.Navigator screenOptions={{headerShown: false}}>
        <StackNav.Screen name="Login" component={Login} />
        <StackNav.Screen name="Register" component={Register} />
        <StackNav.Screen name="Forgot" component={Forgot} />
        <StackNav.Screen name="BotTabs" component={BotTabs} />
        <StackNav.Screen name="Home" component={Home} />
      </StackNav.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default App;
