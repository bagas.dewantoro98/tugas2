import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

export default function Home({navigation}) {
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('../big_banner.png')}
              style={{flex: 1, resizeMode: 'cover', aspectRatio: 2.5}}
            />
          </View>
          <View
            style={{
              marginTop: 20,
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <View style={styles.logo}>
              <Image source={require('../un_ferry.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('../id_ferry.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('../tent.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('../flag.png')} />
            </View>
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: '#2e3283',
              marginTop: 370,
              alignItems: 'center',
              justifyContent: 'center',
              marginHorizontal: 130,
              height: 50,
              marginBottom: 50,
              borderRadius: 10,
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#ffffff',
                fontWeight: 'bold',
                // justifyContent: 'center',
                // alignItems: 'center',
              }}>
              More ...
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {flex: 1, alignItems: 'center'},
});
