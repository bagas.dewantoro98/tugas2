import React from 'react';
import type {Node} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default function Login({navigation}) {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // position: 'relative',
      }}>
      <ScrollView>
        <View style={{...styles.content}}>
          <Image source={require('../logo.png')} style={{marginBottom: 20}} />
          <View style={styles.toptext}>
            <Text style={styles.texttop}>Please sign in to continue</Text>
          </View>
          <TextInput style={styles.name} placeholder="Username" />
          <TextInput
            style={styles.email}
            secureTextEntry={true}
            placeholder="Password"
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('BotTabs')}>
            <Text style={styles.buttontext}>SIGN IN</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.forgot}
            onPress={() => navigation.navigate('Forgot')}>
            <Text style={styles.textforgot}>Forgot Password</Text>
          </TouchableOpacity>
          <View
            style={{flexDirection: 'row', marginTop: 40, alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <Text
              style={{
                minWidth: 50,
                textAlign: 'center',
                paddingLeft: 14,
                paddingRight: 14,
                ...styles.textforgot,
              }}>
              Login With
            </Text>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
        </View>

        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            marginBottom: 50,
            // backgroundColor: 'red',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              marginBottom: 33,
            }}>
            <Image
              source={require('../facebook.png')}
              style={{width: 46, height: 46, marginRight: 38}}
            />
            <View
              style={{backgroundColor: 'white', borderRadius: 999, padding: 5}}>
              <Image
                source={require('../google.png')}
                style={{width: 46, height: 46}}
              />
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
            }}>
            <Text style={{...styles.footertext, marginRight: 38}}>
              App Version
            </Text>
            <Text style={styles.footertext}>2.8.3</Text>
          </View>
        </View>
      </ScrollView>

      <View style={styles.footer}>
        <Text style={styles.footertext}>Don't have account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={styles.footertext1}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    // flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
  },

  content: {
    width: '100%',
    alignItems: 'center',
    padding: 50,
  },

  footer: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    justifyContent: 'center',
    height: 40,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.51,
    shadowRadius: 13.16,

    elevation: 20,
  },

  photo: {marginBottom: 20},

  toptext: {marginBottom: 35},

  name: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    fontSize: 13,
  },

  email: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    fontSize: 13,
    marginTop: 20,
  },

  button: {
    backgroundColor: '#2E3283',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    marginTop: 20,
    alignItems: 'center',
  },

  forgot: {marginTop: 20},

  footertext: {color: '#5B5B5B', fontSize: 13},
  footertext1: {color: '#5B5B5B', fontSize: 13, fontWeight: 'bold'},
  texttop: {color: '#5B5B5B', fontSize: 19},
  buttontext: {color: 'white', fontSize: 13},
  textforgot: {color: '#2E3283'},
});
