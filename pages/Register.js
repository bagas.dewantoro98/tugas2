import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';

export default function Register({navigation}) {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
      }}>
      <ScrollView
        contentContainerStyle={{
          flexDirection: 'column',
        }}>
        <View
          style={{
            display: 'flex',
            position: 'relative',
            padding: 40,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image source={require('../logo.png')} style={styles.photo} />
          <View style={styles.toptext}>
            <Text style={styles.texttop}>Create an Account</Text>
          </View>
          <TextInput style={styles.name} placeholder="Name" />
          <TextInput style={styles.email} placeholder="Email" />
          <TextInput style={styles.email} placeholder="Phone" />
          <TextInput style={styles.email} placeholder="Password" />
          <TouchableOpacity style={{...styles.button}}>
            <Text style={styles.buttontext}>SIGN UP</Text>
          </TouchableOpacity>
        </View>
        {/* </View> */}
      </ScrollView>
      <View style={{...styles.footer}}>
        <Text style={styles.footertext}>Already have account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.footertext1}>Log in</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    // flex: 1,
    // height: '100%',
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    // marginBottom: 40,
  },

  content: {
    width: '100%',
    alignItems: 'center',
    padding: 50,
  },

  footer: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    justifyContent: 'center',
    height: 40,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.51,
    shadowRadius: 13.16,

    elevation: 20,
  },

  photo: {marginBottom: 20},

  toptext: {marginBottom: 35},

  name: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    fontSize: 13,
  },

  email: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    fontSize: 13,
    marginTop: 20,
  },

  button: {
    backgroundColor: '#2E3283',
    borderRadius: 5,
    width: '100%',
    height: 50,
    paddingVertical: 16,
    paddingHorizontal: 16,
    marginTop: 20,
    alignItems: 'center',
  },

  footertext: {color: '#5B5B5B', fontSize: 13},
  footertext1: {color: '#5B5B5B', fontSize: 13, fontWeight: 'bold'},
  texttop: {color: '#5B5B5B', fontSize: 19},
  buttontext: {color: 'white', fontSize: 13},
});
